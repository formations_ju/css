---
theme : "night"
transition: "slide"
highlightTheme: "monokai"
logoImg: "css.png"
slideNumber: true
title: "VSCode Reveal intro"
---

## Formation CSS

---

## Etapes

- présentation <small>trés rapide</small>
- structure <small>encore de la théorie</small>


- projet <small>toujours plus haut</small>

---

## Présentation

CSS -> Cascading Style Sheet

---

## Structure

```css
h1{
    color:#FF0000;
    font-size:24px;
}
```

h1 -> sélecteur

color -> propriété

#FF0000 -> valeur

---

## Ou trouve-t-on le css

trés proche du html<sup>(trop proche)</sup>
```html
<h1 style="color:#FF0000;font-size:24px">mon gros titre</h1>
```
Dans la bonne balise<sup>(t'as le style ou tu l'as pas)</sup>
```html
<style>
    h1{
        color:#FF0000;
        font-size:24px;
    }
</style>
```
Dans le bon fichier<sup>(le fichier css)</sup>
```html
<link href="style.css" rel="stylesheet">
```

---

## Sélecteurs

- selecteur de type -> h1, a, table,...
- selecteur de classe -> .nomDeClasse
- selecteur d'identifiant -> #Id
- selecteur d'attribut -> h1[attribut]

__combinaison de sélecteur__

```<space>```: combinaison descendante 

```>```: combinaison descendante directe

sans oublier les pseudo-classes.

---

Exercices

[css diner](https://flukeout.github.io/)

---

## tout est dans la boite

__bloc vs inline__

padding, border et margin

#### complement

<small>fusion de marge</small>

---

Encore de la [doc](https://devdocs.io/)

---





